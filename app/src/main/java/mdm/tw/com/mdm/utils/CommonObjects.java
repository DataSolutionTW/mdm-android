package mdm.tw.com.mdm.utils;

import android.content.Context;


/**
 */
public class CommonObjects {
    private static Context context;
    private static String codeFromServer;

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        CommonObjects.context = context;
    }


    public static String getCodeFromServer() {
        return codeFromServer;
    }

    public static void setCodeFromServer(String codeFromServer) {
        CommonObjects.codeFromServer = codeFromServer;
    }
}
