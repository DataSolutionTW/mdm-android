package mdm.tw.com.mdm.helpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import mdm.tw.com.mdm.services.SyncDataWithServer;

/***********************************************
 * Created by waqas on 23/05/18.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {
    Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        int status = NetworkUtil.getConnectivityStatus(context);
        if(status != 0){
            if(!UtilsFunctions.isServiceRunning(SyncDataWithServer.class, context)) {
                Intent serviceIntent = new Intent(context, SyncDataWithServer.class);
                context.startService(serviceIntent);
            }
        }


        Log.e("Receiver ", "" + status);

//        if (status.equals(Constants.NOT_CONNECT)) {
//            Log.e("Receiver ", "not connection");// your code when internet lost
//
//        } else {
//            Log.e("Receiver ", "connected to internet");//your code when internet connection come back
//        }

//        MainActivity.addLogText(status);

    }
}
