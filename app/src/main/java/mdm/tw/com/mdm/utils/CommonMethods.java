package mdm.tw.com.mdm.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import java.net.InetAddress;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mdm.tw.com.mdm.R;

public class CommonMethods {
    private static ProgressDialog pgDialog;

    public static void turnGPSOn(Context activity)
    {
        Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", true);
        activity.sendBroadcast(intent);

    }

    // automatic turn off the gps
    public static void turnGPSOff(Context activity)
    {
        Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", false);
        activity.sendBroadcast(intent);


    }

    public static void showProgressDialog(Context nContext) {
        try {
            pgDialog = new ProgressDialog(nContext);
            pgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            pgDialog.setIndeterminate(true);
            pgDialog.setCancelable(false);
            pgDialog.show();
            pgDialog.setContentView(R.layout.dialog_loading);
        } catch (Exception e) {
        }
    }

    public static void hideProgressDialog() {
        try {
            if (pgDialog != null)
                pgDialog.dismiss();
        } catch (Exception e) {

        }
    }

//    public static void showToast(String message, int toastDuration) {
//        try {
//            View v = createView(CommonObjects.getContext(), R.layout.toast_layout, null);
//            TextView nTextView = (TextView) v.findViewById(R.id.tvToast);
//            nTextView.setText(message);
//            Toast toast = Toast.makeText(CommonObjects.getContext(), message, toastDuration);
//            toast.setGravity(Gravity.CENTER, 0, 0);
//            toast.setView(v);
//            toast.show();
//        } catch (Exception e) {
//        }
//    }

    public static void hideSoftKeyboard(View v, Context context) {
        try {
            InputMethodManager imm = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public static View createView(Context context, int layout, ViewGroup parent) {
        try {
            LayoutInflater newLayoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            return newLayoutInflater.inflate(layout, parent, false);
        } catch (Exception e) {
            Log.i("IN CATCH ", e.toString());
            return null;
        }
    }

    public static String getStringPreference(Context nContext,
                                             String preferenceName, String preferenceItem, String defaultValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName,
                    Context.MODE_PRIVATE);
            return nPreferences.getString(preferenceItem, defaultValue);
        } catch (Exception e) {
            return "";
        }
    }

    public static int getIntPreference(Context nContext,
                                       String preferenceName, String preferenceItem, int deafaultValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName,
                    Context.MODE_PRIVATE);
            return nPreferences.getInt(preferenceItem, deafaultValue);
        } catch (Exception e) {
            return deafaultValue;
        }
    }

    public static Boolean getBooleanPreference(Context nContext,
                                               String preferenceName, String preferenceItem,
                                               Boolean defaultValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName,
                    Context.MODE_PRIVATE);
            return nPreferences.getBoolean(preferenceItem, defaultValue);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static void setStringPreference(Context nContext,
                                           String preferenceName, String preferenceItem,
                                           String preferenceItemValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName,
                    Context.MODE_PRIVATE);
            Editor nEditor = nPreferences.edit();
            nEditor.putString(preferenceItem, preferenceItemValue);
            nEditor.commit();
        } catch (Exception e) {
        }
    }

    public static void setIntPreference(Context nContext,
                                        String preferenceName, String preferenceItem,
                                        int preferenceItemValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName,
                    Context.MODE_PRIVATE);
            Editor nEditor = nPreferences.edit();
            nEditor.putInt(preferenceItem, preferenceItemValue);
            nEditor.commit();
        } catch (Exception e) {
        }
    }

    public static void setBooleanPreference(Context nContext,
                                            String preferenceName, String preferenceItem,
                                            Boolean preferenceItemValue) {
        try {
            SharedPreferences nPreferences;
            nPreferences = nContext.getSharedPreferences(preferenceName,
                    Context.MODE_PRIVATE);
            Editor nEditor = nPreferences.edit();
            nEditor.putBoolean(preferenceItem, preferenceItemValue);
            nEditor.commit();
        } catch (Exception e) {
        }
    }

//    public static boolean isNetworkAvailable(Context nContext) {
//        try {
//            ConnectivityManager connectivityManager = (ConnectivityManager) nContext
//                    .getSystemService(Context.CONNECTIVITY_SERVICE);
//            NetworkInfo activeNetworkInfo = connectivityManager
//                    .getActiveNetworkInfo();
//            return activeNetworkInfo != null;
//        } catch (Exception e) {
//        }
//        return false;
//    }

//    public static boolean isWifiConnected(Context nContext) {
//        try {
//            ConnectivityManager connectivityManager = (ConnectivityManager) nContext
//                    .getSystemService(Context.CONNECTIVITY_SERVICE);
//            NetworkInfo networkInfo = null;
//            if (connectivityManager != null) {
//                networkInfo = connectivityManager
//                        .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
//            }
//            return networkInfo == null ? false : networkInfo.isConnected();
//        } catch (Exception e) {
//        }
//        return false;
//    }

    public static boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            if (ipAddr.equals("")) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static int getDeviceWidth(Context nConext) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) nConext).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;
    }

    public static int getDeviceHeight(Context nConext) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) nConext).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.heightPixels;
    }

    public static boolean isEmailValid(String email) {
        try {
            Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
            Matcher matcher = pattern.matcher(email);
            return matcher.matches();
        } catch (Exception e) {
            return false;
        }
    }



    public static String getTodaysDate() {
        Calendar date = Calendar.getInstance();
        String hour = date.get(Calendar.HOUR_OF_DAY) + "";
        String minute = date.get(Calendar.MINUTE) + "";
        if (hour.length() == 0) {
            hour = "0" + hour;
        }
        if (minute.length() == 0) {
            minute = "0" + minute;
        }
        //String dateToday = getDay(date.get(Calendar.DAY_OF_WEEK)) + ", " + date.get(Calendar.DAY_OF_MONTH) + " " + getMonth(date.get(Calendar.MONTH)) + " " + hour + ":" + minute + ", " + date.get(Calendar.YEAR);
        String dateToday = /*getDay(date.get(Calendar.DAY_OF_WEEK)) + ", " + */date.get(Calendar.DAY_OF_MONTH) + "-" + getMonth(date.get(Calendar.MONTH)) + "-" + date.get(Calendar.YEAR) + " " + hour + ":" + minute;
        return dateToday;
    }

    public static String getTodaysDateForServer() {
        Calendar date = Calendar.getInstance();
        String hour = date.get(Calendar.HOUR_OF_DAY) + "";
        String minute = date.get(Calendar.MINUTE) + "";
        String seconds = date.get(Calendar.SECOND) + "";
        //String am_pm = date.get(Calendar.AM_PM) + "";
        String dayOfMonth = date.get(Calendar.DAY_OF_MONTH) + "";

        /*if (am_pm == "0") {
            am_pm = "AM";
        } else {
            am_pm = "PM";
        }*/

        if (hour.length() == 0) {
            hour = "0" + hour;
        }
        if (minute.length() == 1) {
            minute = "0" + minute;
        }
        if (dayOfMonth.length() == 1) {
            dayOfMonth = "0" + dayOfMonth;
        }


        //String dateToday = getDay(date.get(Calendar.DAY_OF_WEEK)) + ", " + date.get(Calendar.DAY_OF_MONTH) + " " + getMonth(date.get(Calendar.MONTH)) + " " + hour + ":" + minute + ", " + date.get(Calendar.YEAR);
        String dateToday = getMonthInNum(date.get(Calendar.MONTH)) + "/" + dayOfMonth + "/" + date.get(Calendar.YEAR) + " " + hour + ":" + minute + ":" + seconds;// + " " + am_pm;
        return dateToday;
    }

    public static String getDay(int id) {
        switch (id) {
            case 1:
                return "Sunday";

            case 2:
                return "Monday";

            case 3:
                return "Tuesday";

            case 4:
                return "Wednesday";

            case 5:
                return "Thursday";

            case 6:
                return "Friday";

            case 7:
                return "Saturday";

        }

        return "";
    }

    public static String getMonth(int id) {
        switch (id + 1) {
            case 1:
                return "January";

            case 2:
                return "February";

            case 3:
                return "March";

            case 4:
                return "Aprail";

            case 5:
                return "May";

            case 6:
                return "June";

            case 7:
                return "July";

            case 8:
                return "August";

            case 9:
                return "September";

            case 10:
                return "Octtobar";

            case 11:
                return "November";

            case 12:
                return "December";
        }

        return "";
    }

    public static String getMonthInNum(int id) {
        switch (id + 1) {
            case 1:
                return "01";

            case 2:
                return "02";

            case 3:
                return "03";

            case 4:
                return "04";

            case 5:
                return "05";

            case 6:
                return "06";

            case 7:
                return "07";

            case 8:
                return "08";

            case 9:
                return "09";

            case 10:
                return "10";

            case 11:
                return "11";

            case 12:
                return "12";
        }

        return "";
    }


}
