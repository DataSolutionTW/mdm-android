package mdm.tw.com.mdm.utils;

/**
 * Created by Danii on 25-Mar-16.
 */
public class Constants_Keys {

    public static final long CONNECTION_TIME_OUT_SECONDS = 80;

    public static class KeyValues {
        public static final String USER_DETAILS = "User_Details";
        public static final String USER_NAME_EMAIL = "User_name_email";
        public static final String USER_PASSWORD = "User_Password";

        public static final String AUTHORIZATION_DETAILS = "Auth_Details";
        public static final String REGISTRATION_DETAILS = "Registration_Details";
        public static final String AUTH_TYPE_AND_TOKEN = "Auth_Type_And_Token";
        public static final String ALREADY_ADDED = "Already_Added";
        public static final String TOKEN_EXPIRES_DATE = "token_expires_date";
        public static final String LOCATION_DETAILS = "Location_Details";
        public static final String LOCATION_ACCURACY = "Location_Accuracy";
        public static final String SIMPLE_CODE = "11";
        public static final String BATTER_ALERT = "12";
        public static final String DISTANCE_ALERT = "13";
    }

    public static class USER_METHODS {

        //Live
//      public static String LOGIN_BASE_URl = "http://202.166.174.69/naqshawebapi/Api/";
        public static String BASE_URL = "http://103.8.112.78/MDMWebApi/api/";
        public static String primary_ip = "103.8.112.78";
        public static String s_ip = "103.8.112.78";
        public static String EXTENDED_URL = "/MDMWebApi/api/";
    }
}
