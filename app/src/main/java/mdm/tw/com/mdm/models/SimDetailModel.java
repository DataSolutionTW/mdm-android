package mdm.tw.com.mdm.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Android Developer on 8/24/2017.
 */

public class SimDetailModel {

    @SerializedName("IsValid")
    @Expose
    private Boolean isValid;
    @SerializedName("Message")
    @Expose
    private String message;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @SerializedName("Result")
    @Expose

    private Result result = null;
    @SerializedName("Action")
    @Expose
    private String action;
    @SerializedName("Version")
    @Expose
    private String version;
    @SerializedName("Path")
    @Expose
    private String path;
    public Boolean getIsValid() {
        return isValid;
    }

    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }


    public class Result {

        @SerializedName("ID")
        @Expose
        private Integer iD;
        @SerializedName("AppVersion")
        @Expose
        private String appVersion;
        @SerializedName("OSVersion")
        @Expose
        private String oSVersion;
        @SerializedName("DModel")
        @Expose
        private String dModel;
        @SerializedName("DeviceID")
        @Expose
        private String deviceID;
        @SerializedName("PollInterval")
        @Expose
        private Integer pollInterval;
        @SerializedName("PollAngleChange")
        @Expose
        private Integer pollAngleChange;
        @SerializedName("PollDistanceCovered")
        @Expose
        private Integer pollDistanceCovered;
        @SerializedName("PollBatteryLowLevel")
        @Expose
        private Integer pollBatteryLowLevel;
        @SerializedName("PrimaryIP")
        @Expose
        private String primaryIP;
        @SerializedName("SecondaryIP")
        @Expose
        private String secondaryIP;
        @SerializedName("IMSI")
        @Expose
        private String iMSI;
        @SerializedName("AndroidID")
        @Expose
        private String androidID;
        @SerializedName("Version")
        @Expose
        private String version;
        @SerializedName("Path")
        @Expose
        private String path;

        public Integer getID() {
            return iD;
        }

        public void setID(Integer iD) {
            this.iD = iD;
        }

        public String getAppVersion() {
            return appVersion;
        }

        public void setAppVersion(String appVersion) {
            this.appVersion = appVersion;
        }

        public String getOSVersion() {
            return oSVersion;
        }

        public void setOSVersion(String oSVersion) {
            this.oSVersion = oSVersion;
        }

        public String getDModel() {
            return dModel;
        }

        public void setDModel(String dModel) {
            this.dModel = dModel;
        }

        public String getDeviceID() {
            return deviceID;
        }

        public void setDeviceID(String deviceID) {
            this.deviceID = deviceID;
        }

        public Integer getPollInterval() {
            return pollInterval;
        }

        public void setPollInterval(Integer pollInterval) {
            this.pollInterval = pollInterval;
        }

        public Integer getPollAngleChange() {
            return pollAngleChange;
        }

        public void setPollAngleChange(Integer pollAngleChange) {
            this.pollAngleChange = pollAngleChange;
        }

        public Integer getPollDistanceCovered() {
            return pollDistanceCovered;
        }

        public void setPollDistanceCovered(Integer pollDistanceCovered) {
            this.pollDistanceCovered = pollDistanceCovered;
        }

        public Integer getPollBatteryLowLevel() {
            return pollBatteryLowLevel;
        }

        public void setPollBatteryLowLevel(Integer pollBatteryLowLevel) {
            this.pollBatteryLowLevel = pollBatteryLowLevel;
        }

        public String getPrimaryIP() {
            return primaryIP;
        }

        public void setPrimaryIP(String primaryIP) {
            this.primaryIP = primaryIP;
        }

        public String getSecondaryIP() {
            return secondaryIP;
        }

        public void setSecondaryIP(String secondaryIP) {
            this.secondaryIP = secondaryIP;
        }

        public String getIMSI() {
            return iMSI;
        }

        public void setIMSI(String iMSI) {
            this.iMSI = iMSI;
        }

        public String getAndroidID() {
            return androidID;
        }

        public void setAndroidID(String androidID) {
            this.androidID = androidID;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

    }
}
