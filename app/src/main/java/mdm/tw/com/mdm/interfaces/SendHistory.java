package mdm.tw.com.mdm.interfaces;

import java.util.Map;

import mdm.tw.com.mdm.models.RegisterResponse;
import mdm.tw.com.mdm.models.SimDetailModel;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by TW on 7/6/2017.
 */

public interface SendHistory {

    @FormUrlEncoded
    @POST("MVTLandC/AddMVTLandC")
    Call<RegisterResponse> sendHistoryData(@FieldMap Map<String, String> names);

    @GET("Device/GetDeviceByID")
    Call<SimDetailModel> getImei(@Query("DeviceID") String names, @Query("IMSI") String IMSI, @Query("AndroidID") String AndroidID,
                                 @Query("appVersion") String appVersion, @Query("oSVersion") String oSVersion, @Query("dModel") String dModel);
}
