package mdm.tw.com.mdm.helpers;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellInfo;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import mdm.tw.com.mdm.services.BatteryChangeReceiver;
import mdm.tw.com.mdm.utils.Constants_Keys;
import okhttp3.OkHttpClient;

import static mdm.tw.com.mdm.helpers.Constants.CONNECTIVITY_ACTION;

/**
 * Created by developer on 5/22/18.
 */

public class UtilsFunctions {

    public static String getVersionName(Context context){
        String version = "";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return version;
    }

    public static String getSimSerialNumber(Context context){

        String simSerialNumbrer = "";
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);

            if (telephonyManager.getSimSerialNumber() == null) {

                return simSerialNumbrer;
            } else {
                simSerialNumbrer = telephonyManager.getSimSerialNumber();
            }
        }catch (SecurityException e){
            e.printStackTrace();
        }

        return simSerialNumbrer;

    }

    public static String getDeviceName(){

        return android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL;

    }

    public static String getSid(){
        BufferedReader br;
        String line;
        String cid1;
        String sid1 = "";
        String sid1part1;
        String sid1part2;

        File fileread = new File("/sys/block/mmcblk0/../../cid");
        if (fileread.exists()) {
            StringBuilder text = new StringBuilder();
            try {
                br = new BufferedReader(new FileReader(fileread));
                while (true) {
                    line = br.readLine();
                    if (line == null) {
                        break;
                    }
                    cid1 = line;
                    sid1part1 = cid1.substring(0, 2);
                    sid1part2 = cid1.substring(18, 26);
                    sid1 = sid1part1 + sid1part2;
                    return sid1;
                }
            } catch (IOException e) {
                return sid1;
            }
        }
        return sid1;
    }

    public static OkHttpClient getOkHttpClient(){

        OkHttpClient okHttpClient = null;

        return okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(Constants_Keys.CONNECTION_TIME_OUT_SECONDS, TimeUnit.SECONDS)
                .readTimeout(Constants_Keys.CONNECTION_TIME_OUT_SECONDS, TimeUnit.SECONDS)
                .writeTimeout(Constants_Keys.CONNECTION_TIME_OUT_SECONDS, TimeUnit.SECONDS)
                .build();
    }

    public static int getCurrentApiVersion(){
        return android.os.Build.VERSION.SDK_INT;
    }

    public static void registerBatteryReceiver(Context context, BatteryChangeReceiver receiver){
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        context.registerReceiver(receiver, ifilter);
    }
    public static void unRegisterBatteryStatus(Context context, BatteryChangeReceiver receiver){
        context.unregisterReceiver(receiver);
    }

    public static void registerNetworkReceiver(Context context, NetworkChangeReceiver receiver){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTIVITY_ACTION);
        context.registerReceiver(receiver, intentFilter);
    }

    public static void unRegisterNetworkReceiver(Context context, NetworkChangeReceiver networkChangeReceiver){
        context.unregisterReceiver(networkChangeReceiver);
    }


    public static String getCellInfo(Context ctx){
        TelephonyManager tel = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
        int phoneTypeInt = tel.getPhoneType();

        System.out.println("phonetypeint : "+phoneTypeInt);

        String cellList ="";

        //from Android M up must use getAllCellInfo
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){

            List<NeighboringCellInfo> neighCells = tel.getNeighboringCellInfo();
            if (neighCells != null) {

                try {
                    for (int i = 0; i < neighCells.size(); i++) {
                        try {
                            JSONObject cellObj = new JSONObject();
                            NeighboringCellInfo thisCell = neighCells.get(i);
                            cellObj.put("cellId", thisCell.getCid());
                            cellObj.put("lac", thisCell.getLac());
                            cellObj.put("rssi", thisCell.getRssi());

                            System.out.println("cellId : " + thisCell.getCid() + "lac : " + thisCell.getLac() + "rssi : " + thisCell.getRssi());

                            cellList = cellList + thisCell.getCid();
//                    cellList=cellList+"lac:"+ thisCell.getLac()+",";
                            cellList = cellList + ":" + thisCell.getRssi() + ",";
                        } catch (Exception e) {
                            Log.d("test", e.getMessage());
                        }
                    }
                } catch (NullPointerException e) {

                    try {
                        JSONObject cellObj = new JSONObject();
//                    NeighboringCellInfo thisCell = neighCells.get(i);
                        cellObj.put("cellId", 0);
                        cellObj.put("lac", 0);
                        cellObj.put("rssi", 0);


                        cellList = cellList + 0;
//                    cellList=cellList+"lac:"+ thisCell.getLac()+",";
                        cellList = cellList + ":" + 0 + ",";
                    } catch (Exception ex) {
                        Log.d("test", ex.getMessage());
                    }

                }
            } else {
                try {
                    JSONObject cellObj = new JSONObject();
//                    NeighboringCellInfo thisCell = neighCells.get(i);
                    cellObj.put("cellId", 0);
                    cellObj.put("lac", 0);
                    cellObj.put("rssi", 0);

                    cellList = cellList + 0;
//                    cellList=cellList+"lac:"+ thisCell.getLac()+",";
                    cellList = cellList + ":" + 0 + ",";
                } catch (Exception ex) {
                    Log.d("test", ex.getMessage());
                }
            }
        }
        } else {
            List<CellInfo> infos = tel.getAllCellInfo();
            for (int i = 0; i<infos.size(); ++i) {
                try {
                    JSONObject cellObj = new JSONObject();
                    CellInfo info = infos.get(i);
                    if (info instanceof CellInfoGsm){
                        CellSignalStrengthGsm gsm = ((CellInfoGsm) info).getCellSignalStrength();
                        CellIdentityGsm identityGsm = ((CellInfoGsm) info).getCellIdentity();
                        cellObj.put("cellId", identityGsm.getCid());
                        cellObj.put("lac", identityGsm.getLac());
                        cellObj.put("dbm", gsm.getDbm());

                        System.out.println("cellId : "+ identityGsm.getCid() + "lac : " + identityGsm.getLac() + "dbm : "+ gsm.getDbm());

                        cellList=cellList+ identityGsm.getCid();
//                        cellList=cellList+"lac:"+ identityGsm.getLac()+",";
                        cellList=cellList+":"+ gsm.getDbm()+",";
                    } else if (info instanceof CellInfoLte) {
                        CellSignalStrengthLte lte = ((CellInfoLte) info).getCellSignalStrength();
                        CellIdentityLte identityLte = ((CellInfoLte) info).getCellIdentity();
                        cellObj.put("cellId", identityLte.getCi());
                        cellObj.put("tac", identityLte.getTac());
                        cellObj.put("dbm", lte.getDbm());
                        cellList=cellList+ identityLte.getCi();
                        System.out.println("cellId : "+ identityLte.getCi() + "lac : " + identityLte.getTac() + "dbm : "+ lte.getDbm());

//                        cellList=cellList+"tac:"+ identityLte.getCi()+",";
                        cellList=cellList+":"+ lte.getDbm()+",";
                    }

                } catch (Exception ex) {

                }
            }
        }

        return cellList;
    }

    static String getDeviceType(TelephonyManager phonyManager, Context context) {


        int phoneType = phonyManager.getPhoneType();
        switch (phoneType) {
            case TelephonyManager.PHONE_TYPE_NONE:
                return "PHONE_TYPE_NONE";

            case TelephonyManager.PHONE_TYPE_GSM:
                return "PHONE_TYPE_GSM";

            case TelephonyManager.PHONE_TYPE_CDMA:
                return "PHONE_TYPE_CDMA";

            default:
                return "UNKNOWN";
        }
    }

    public static boolean isServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
