package mdm.tw.com.mdm.interfaces;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Streaming;

public interface RetrofitInterface {

    @GET("SigmaTrackingApp_2_0.apk")
    @Streaming
    Call<ResponseBody> downloadFile();
}
