package mdm.tw.com.mdm.helpers;

import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;

/**
 * Created by developer on 5/31/18.
 */

public class MyPhoneStateListener extends PhoneStateListener {

    private int signalStrengthPercent=0;
    int MAX_SIGNAL_DBM_VALUE = 31;
    public static final int UNKNOW_CODE = 99;

    /* Get the Signal strength from the provider, each time there is an update */
    @Override
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        super.onSignalStrengthsChanged(signalStrength);

        if (null != signalStrength && signalStrength.getGsmSignalStrength() != UNKNOW_CODE) {
            signalStrengthPercent = calculateSignalStrengthInPercent(signalStrength.getGsmSignalStrength());
            System.out.println("signal strength : " + signalStrengthPercent);

        }
    }

    private int calculateSignalStrengthInPercent(int signalStrength) {
        return (int) ((float) signalStrength / MAX_SIGNAL_DBM_VALUE * 100);
    }
}
