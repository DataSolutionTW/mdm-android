package mdm.tw.com.mdm.interfaces;

/**
 * Created by developer on 6/7/18.
 */

public interface GetRecordValue {

    void showRecordValue(int value);
    void showSpeedValues(String speed);
    void showDistanceValues(String distance);
    void showDirectionValues(String direction);
    void showAltitudeValues(String altitude);
    void showLatLngValues(String latLng);

}
