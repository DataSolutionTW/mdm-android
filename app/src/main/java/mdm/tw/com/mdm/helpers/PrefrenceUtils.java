package mdm.tw.com.mdm.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;

/**
 * Created by developer on 5/28/18.
 */

public class PrefrenceUtils {

    public static void saveImei(Context context, String value){

        SharedPreferences pref = context.getSharedPreferences(Constants.PREF_MDM, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.IMEI, value);
        editor.commit();

    }

    public static String getImei(Context context){

        SharedPreferences pref = context.getSharedPreferences(Constants.PREF_MDM, Context.MODE_PRIVATE);
        return pref.getString(Constants.IMEI, "");
    }

    public static void savePrimaryIP(Context context, String value){

        SharedPreferences pref = context.getSharedPreferences(Constants.PREF_MDM, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.PRIMARY_IP, value);
        editor.commit();

    }

    public static String getPrimaryIP(Context context, String defaultVal){

        SharedPreferences pref = context.getSharedPreferences(Constants.PREF_MDM, Context.MODE_PRIVATE);
        return pref.getString(Constants.PRIMARY_IP, defaultVal);
    }

    public static void saveSecondaryIP(Context context, String value){

        SharedPreferences pref = context.getSharedPreferences(Constants.PREF_MDM, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.SECONDARY_IP, value);
        editor.commit();

    }

    public static String getSecondaryIP(Context context, String defaultVal){

        SharedPreferences pref = context.getSharedPreferences(Constants.PREF_MDM, Context.MODE_PRIVATE);
        return pref.getString(Constants.SECONDARY_IP, defaultVal);
    }

    public static void saveGoogleLocation(Context context,Location location){

        SharedPreferences pref = context.getSharedPreferences(Constants.PREF_MDM, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putFloat(Constants.GOOGLE_LOCATION_LAT, (float)(location.getLatitude()));
        editor.putFloat(Constants.GOOGLE_LOCATION_LONG, (float)(location.getLongitude()));

        editor.commit();

    }

    public static float getGoogleLocLatitude(Context context, float defaultVal){

        SharedPreferences pref = context.getSharedPreferences(Constants.PREF_MDM, Context.MODE_PRIVATE);
        return pref.getFloat(Constants.GOOGLE_LOCATION_LAT, 0);
    }
    public static float getGoogleLocLongitude(Context context, float defaultVal){

        SharedPreferences pref = context.getSharedPreferences(Constants.PREF_MDM, Context.MODE_PRIVATE);
        return pref.getFloat(Constants.GOOGLE_LOCATION_LONG, 0);
    }
}
