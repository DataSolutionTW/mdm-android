package mdm.tw.com.mdm.services;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.BufferedReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;

import mdm.tw.com.mdm.db.DbHelper;
import mdm.tw.com.mdm.helpers.Constants;
import mdm.tw.com.mdm.helpers.NetworkUtil;
import mdm.tw.com.mdm.helpers.PrefrenceUtils;
import mdm.tw.com.mdm.helpers.UtilsFunctions;
import mdm.tw.com.mdm.interfaces.SendHistory;
import mdm.tw.com.mdm.models.Internet;
import mdm.tw.com.mdm.models.LocationItem;
import mdm.tw.com.mdm.models.RegisterResponse;
import mdm.tw.com.mdm.utils.CommonMethods;
import mdm.tw.com.mdm.utils.Constants_Keys;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by TW on 7/3/2017.
 */

public class SyncDataWithServer extends IntentService {
    BufferedReader br;
    String line;
    DbHelper db;
    public static String cid1;
    public static String sid1;
    public static String sid1part1;
    public static String sid1part2;

//    public  final int notify = CommonMethods.getIntPreference(SyncDataWithServer.this,"MyPref","PollInterval",1)* 10 * 1000;  //interval between two services(Here Service run every 5 Minute)
    public  final int notify = 1* 5 * 1000;  //interval between two services(Here Service run every 5 Minute)
    private Handler mHandler = new Handler();   //run on another Thread to avoid crash
    private Timer mTimer = null;    //timer handling
    DbHelper getDatabase;
    private ArrayList<LocationItem> list;
    private Retrofit retrofit,retrofit2;
    int primarycountfailed=0;
    int Secondarycountfailed=0;
    boolean primary=true;
    boolean secondary=true;

    SendHistory service;

    public static final String SYNC_DATA_NOTIFICATION = "mdm.tw.com.mdm.services.receiver";


    public SyncDataWithServer() {

        super("SyncDataWithServer");
    }


    @Override
    public void onCreate() {
        super.onCreate();

        getDatabase = DbHelper.getInstance(getApplicationContext());
         String url = "http://"+ CommonMethods.getStringPreference(SyncDataWithServer.this, Constants.PREF_MDM, Constants.PRIMARY_IP,"");
        System.out.println("url is : "+ url);

        OkHttpClient okHttpClient = UtilsFunctions.getOkHttpClient();
                 retrofit = new Retrofit.Builder()
                    .baseUrl("http://"+ CommonMethods.getStringPreference(SyncDataWithServer.this, Constants.PREF_MDM, Constants.PRIMARY_IP,"")
                            + Constants_Keys.USER_METHODS.EXTENDED_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
            retrofit2 = new Retrofit.Builder()
                    .baseUrl("http://"+CommonMethods.getStringPreference(SyncDataWithServer.this,Constants.PREF_MDM, Constants.SECONDARY_IP,"")
                            +Constants_Keys.USER_METHODS.EXTENDED_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();


            service = retrofit.create(SendHistory.class);

//        }
    }
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        //TODO do something useful
//        return Service.START_STICKY;
//    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        System.out.println("Sync data service");


        list=new ArrayList<LocationItem>();
        if(getDatabase.getTableRowsCount() > 0) {
            System.out.println("Sync getDatabase.getTableRowsCount() : "+ getDatabase.getTableRowsCount());
            list = getDatabase.getAllPointsFromLog_History();

            System.out.println("Sync onHandleIntent List size : "+list.size());

                for(int i = 0; i<list.size(); i++) {
                    if (NetworkUtil.getConnectivityStatus(getApplicationContext()) != 0) {
                        System.out.println("Sync data service ID : " + list.get(i).getId());
                        System.out.println("Sync data service REASON : " + list.get(i).getReason());

                        uploadData(list.get(i));
                    } else {
                        break;

                    }
                }

//            Intent notiIntent = new Intent(SYNC_DATA_NOTIFICATION);
//            notiIntent.putExtra("count", 1);
//            sendBroadcast(notiIntent);

        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        getDatabase.closeDB();
//        mTimer.cancel();    //For Cancel Timer
     //   Toast.makeText(this, "Service is Destroyed", Toast.LENGTH_SHORT).show();

    }

//    class TimeDisplay extends TimerTask {
//        @Override
//        public void run() {
//            // run on another thread
//            mHandler.post(new Runnable() {
//                @Override
//                public void run() {
//                    // display toast
//
//                    list=new ArrayList<LocationItem>();
//                    if(!db.isLogEmpty_location()){
//                        list=db.getAllPointsFromLog_History();
//                        list.size();
//
////                        for(int a=0;a<list.size();a++){
//                            if(list.size()>0&& ExtApi.isNetworkConnected(SyncDataWithServer.this)) {
//
////                                Toast.makeText(getApplicationContext(), " not Available Service 3 ", Toast.LENGTH_SHORT).show();
//                                uploadData(list.get(0));
//                         //       db.updateflag();
////                            }
//                        }
////                        stopService(new Intent(SyncDataWithServer.this, MyService.class));
////                        startService(new Intent(SyncDataWithServer.this, MyService.class));
//                    }
////                    CommonMethods.showProgressDialog(this);
//
//                }
//
//
//
//            });
//        }

    public String getMacAddr() {

        String a="";
        @SuppressLint("WifiManagerLeak") WifiManager wifiMan = (WifiManager) getSystemService(
                Context.WIFI_SERVICE);

        WifiInfo wifiInf = wifiMan.getConnectionInfo();
        if (wifiMan.isWifiEnabled() == false)
        {
//            Toast.makeText(getApplicationContext(), "wifi is disabled..making it enabled", Toast.LENGTH_LONG).show();
          //  wifiMan.setWifiEnabled(true);
        }
        else {
            List<ScanResult> results = wifiMan.getScanResults();
            for (ScanResult result : results) {
                if (a.equals("")) {
                    a = a + result.SSID + " | " + result.BSSID + " | " + result.level;
                } else {
                    a = a + " , " + result.SSID + " | " + result.BSSID + " | " + result.level;
                }
            }
        }
        return a;
    }

   void uploadData(final LocationItem locationItem)
    {

        String imei = "" + PrefrenceUtils.getImei(getApplicationContext());//+ telephonyManager.getDeviceId();
        String imsi ="";// telephonyManager.getSubscriberId();
        Date date=new Date();
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");

        String dat = dateFormat.format(date);

//        ArrayList<Internet> dataUseage = getDatabase.getDataUseage(dat);
        HashMap map = new HashMap<String, String>();

        System.out.println("Current Date : "+locationItem.getDatee() + "DeviceID : " + imei);
        ArrayList<Internet> dataUseage = db.getDataUseage(dat);
        map.put("DeviceID", imei);
        map.put("Latitude", locationItem.getLat());
        map.put("Longitude", locationItem.getLongi());
        map.put("Altitude",locationItem.getAltitude());
        map.put("Reason", locationItem.getReason());
        map.put("Speed", locationItem.getSpeed());
        map.put("Direction",   ""+(int) Float.parseFloat( locationItem.getDirection()));
        map.put("Fix",locationItem.getGPSFix());
        map.put("NOS", locationItem.getNOS());
        map.put("HDOP", locationItem.getHDOP());
        map.put("UDT", locationItem.getDatee());
        map.put("GPSDT", locationItem.getDatee());
        map.put("RCode", locationItem.getRcode());
        map.put("Odometer", "");
        map.put("IMSI", imsi);
        map.put("BatteryPercent", ""+locationItem.getBatteryPercent());
        map.put("Server", "MDMWeb Api");
        map.put("GSMCellID", locationItem.getGSMCellID());
        map.put("WifiRouter", getMacAddr());
//        if(dataUseage.size()>0)
        map.put("DataUsage",""+ dataUseage.get(0).getData());



        System.out.println("Get Datee form DB date time : "+locationItem.getDatee());

//        Toast.makeText(getApplicationContext(), " Available Service 3 ", Toast.LENGTH_SHORT).show();

        Call<RegisterResponse> tokenResponseCall = service.sendHistoryData(map);
        tokenResponseCall.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                int statusCode = response.code();
                if (statusCode == 200) {

                    System.out.println("statusCode : "+statusCode);

//                    Toast.makeText(getApplicationContext(), response.body().getResult(), Toast.LENGTH_SHORT).show();
//                        TokenResponse tokenResponse = response.body();

//                        Log.i(TAG, "Get Expires In: " + String.valueOf(tokenResponse.getExpires_in()));

                    getDatabase.deleteRow(locationItem.getId());

                    Intent notiIntent = new Intent(SYNC_DATA_NOTIFICATION);
                    notiIntent.putExtra("count", getDatabase.getTableRowsCount());
                    sendBroadcast(notiIntent);

//                    Toast.makeText(getApplicationContext(), " deleted ", Toast.LENGTH_SHORT).show();
                    Secondarycountfailed=0;
                    primarycountfailed=0;
//                    list=new ArrayList<LocationItem>();

//                        for(int a=0;a<list.size();a++){

//                    list.remove(0);
//                        if(list.size()>0&&ExtApi.isNetworkConnected(SyncDataWithServer.this)) {
//
//                       //     call(list.get(0));
////                            }
//                        }
//
//                } else {
//                    if(list.size()>0&&ExtApi.isNetworkConnected(SyncDataWithServer.this)) {
//
//                     //   call(list.get(0));
//                    }
                   // CommonMethods.hideProgressDialog();
//                    Toast.makeText(SyncDataWithServer.this, "FAILED" + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Log.i("FAIL: ", "ON FAIL: " + t.getMessage());
                if(primary){
                    primarycountfailed++;
                    if (primarycountfailed>5){
                        service=retrofit2.create(SendHistory.class);
                        primary=false;
                        secondary=true;
                    }
                }
                else if(secondary) {
                    Secondarycountfailed++;
                         if (Secondarycountfailed > 5) {
                             primary=true;
                             secondary=false;
                             service = retrofit.create(SendHistory.class);
                            }
                }
             //   call(list.get(0));
                //Toast.makeText(SyncDataWithServer.this, "FAILED" + t.getMessage(), Toast.LENGTH_SHORT).show();
                //CommonMethods.hideProgressDialog();
            }
        });
    }
}
